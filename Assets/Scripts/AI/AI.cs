﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateStuff;

public class AI : MonoBehaviour
{
    public bool switchState = false;
    public float gameTimer;
    public int seconds = 0;

    public StateMachine<AI> stateMachine { get; set; }

    public float detectionRange;
    public float meleeRange;
    public bool isIdle = false;
    public bool isPatrol = false;
    public bool isChase = false;
    public bool isAttack = false;
    public Transform player;

    public float speed;

    public Transform[] moveSpots;
    public int randomSpot;

    private float waitTime;
    public float startWaitTime;

    private float normalSpeed;

    private Animator animator;

    private Quaternion aiRot;
    public float rotSpeed;
    private Vector3 lookAtTarget;
    private Vector3 target;

    public int fixSpot;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        animator = GetComponent<Animator>();

        normalSpeed = speed;

        stateMachine = new StateMachine<AI>(this);
        stateMachine.ChangeState(IdleState.Instance);
        gameTimer = Time.time;

    }

    private void Update()
    {
        speed = normalSpeed;
        if (isIdle)
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Walk", false);
            animator.SetBool("Run", false);
        }

        if (isPatrol)
        {
            animator.SetBool("Walk", true);
            animator.SetBool("Run", false);
            animator.SetBool("Idle", false);
            transform.position = Vector3.MoveTowards(transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);
            VectorRotate(moveSpots[randomSpot].position);

            //Debug.Log("Spot: " + randomSpot);
        }

        else if (isChase)
        {
            //Debug.Log("Chasing");
            animator.SetBool("Run", true);
            animator.SetBool("Idle", false);
            animator.SetBool("Walk", false);
            animator.SetBool("Attack", false);
            speed = normalSpeed + 1.5f;

            transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);

            TransformRotate(player);
        }

        else if (isAttack)
        {
            animator.SetBool("Attack", true);
            animator.SetBool("Run", false);
            animator.SetBool("Idle", false);
            animator.SetBool("Walk", false);
        }

        stateMachine.Update();
    }

    private void TransformRotate(Transform target)
    {
        Vector3 direction = target.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotSpeed * Time.deltaTime);
    }

    public void VectorRotate(Vector3 target)
    {
        Vector3 direction = target - transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = rotation;
    }
}
