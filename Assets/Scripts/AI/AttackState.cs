﻿using UnityEngine;
using StateStuff;

public class AttackState : State<AI>
{
    private static AttackState _instance;

    private AttackState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static AttackState Instance
    {
        get
        {
            if (_instance == null)
            {
                new AttackState();
            }

            return _instance;
        }
    }

    public override void EnterState(ref AI _owner)
    {
        _owner.isAttack = true;
       //Debug.Log("Attacking");
    }

    public override void ExitState(ref AI _owner)
    {
        //Debug.Log("Exiting Attack State");
    }

    public override void UpdateState(ref AI _owner)
    {
        if (Vector3.Distance(_owner.player.position, _owner.transform.position) >= _owner.meleeRange)
        {
            _owner.switchState = !_owner.switchState;
            _owner.isChase = true;
            _owner.stateMachine.ChangeState(ChaseState.Instance);
        }
    }
}
