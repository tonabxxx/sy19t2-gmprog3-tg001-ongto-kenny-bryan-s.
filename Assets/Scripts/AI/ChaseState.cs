﻿using UnityEngine;
using StateStuff;

public class ChaseState : State<AI>
{
    private static ChaseState _instance;

    private ChaseState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static ChaseState Instance
    {
        get
        {
            if (_instance == null)
            {
                new ChaseState();
            }

            return _instance;
        }
    }

    public override void EnterState(ref AI _owner)
    {
        _owner.isChase = true;
        //Debug.Log("Chasing");
    }

    public override void ExitState(ref AI _owner)
    {
        _owner.isIdle = false;
        _owner.isPatrol = false;
        _owner.isChase = false;
        _owner.isAttack = false;
        //Debug.Log("Exiting Chase State");
    }

    public override void UpdateState(ref AI _owner)
    {
        if (Vector3.Distance(_owner.player.position, _owner.transform.position) >= _owner.detectionRange + 3)
        {
            _owner.switchState = !_owner.switchState;
            _owner.stateMachine.ChangeState(IdleState.Instance);
        }

        else if (Vector3.Distance(_owner.player.position, _owner.transform.position) <= _owner.meleeRange)
        {
            _owner.switchState = !_owner.switchState;
            _owner.stateMachine.ChangeState(AttackState.Instance);
        }
    }
}
