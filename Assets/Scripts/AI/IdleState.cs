﻿using UnityEngine;
using StateStuff;

public class IdleState : State<AI>
{
    private static IdleState _instance;

    private IdleState()
    {
        if(_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static IdleState Instance
    {
        get
        {
            if(_instance == null)
            {
                new IdleState();
            }

            return _instance;
        }
    }

    public override void EnterState(ref AI _owner)
    {
        _owner.isIdle = true;
        //Debug.Log("Idling");
    }

    public override void ExitState(ref AI _owner)
    {
        _owner.isIdle = false;
        _owner.isPatrol = false;
        _owner.isChase = false;
        _owner.isAttack = false;
        //Debug.Log("Exiting Idle State");
    }

    public override void UpdateState(ref AI _owner)
    {
        if (Time.time > _owner.gameTimer + 1)
        {
            _owner.gameTimer = Time.time;
            _owner.seconds++;
            //Debug.Log(_owner.seconds);
        }

        if (_owner.seconds == 5)
        {
            _owner.seconds = 0;
            _owner.stateMachine.ChangeState(PatrolState.Instance);
            _owner.switchState = !_owner.switchState;
        }



        if (Vector3.Distance(_owner.player.position, _owner.transform.position) <= _owner.detectionRange)
        {
            _owner.switchState = !_owner.switchState;
            _owner.stateMachine.ChangeState(ChaseState.Instance);
        }


        //if (_owner.switchState)
        //{
        //    _owner.stateMachine.ChangeState(ChaseState.Instance);
        //}
    }
}
