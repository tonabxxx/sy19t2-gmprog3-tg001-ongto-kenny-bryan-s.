﻿using UnityEngine;
using StateStuff;

public class PatrolState : State<AI>
{
    private static PatrolState _instance;

    private PatrolState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static PatrolState Instance
    {
        get
        {
            if (_instance == null)
            {
                new PatrolState();
            }

            return _instance;
        }
    }

    public override void EnterState(ref AI _owner)
    {
        _owner.isPatrol = true;
        _owner.randomSpot = Random.Range(0, _owner.moveSpots.Length);

        //_owner.VectorRotate(_owner.moveSpots[_owner.randomSpot].position);

        //Debug.Log("Patrolling");
    }

    public override void ExitState(ref AI _owner)
    {
        _owner.isIdle = false;
        _owner.isPatrol = false;
        _owner.isChase = false;
        _owner.isAttack = false;
        //Debug.Log("Exiting Patrol State");
    }

    public override void UpdateState(ref AI _owner)
    {
        if (Time.time > _owner.gameTimer + 1)
        {
            _owner.gameTimer = Time.time;
            _owner.seconds++;
            //Debug.Log(_owner.seconds);
        }

        if (_owner.seconds == 3)
        {
            _owner.seconds = 0;
            _owner.stateMachine.ChangeState(IdleState.Instance);
            _owner.switchState = !_owner.switchState;
        }

        if (Vector3.Distance(_owner.player.position, _owner.transform.position) <= _owner.detectionRange)
        {
            _owner.switchState = !_owner.switchState;
            _owner.stateMachine.ChangeState(ChaseState.Instance);
        }
    }
}
