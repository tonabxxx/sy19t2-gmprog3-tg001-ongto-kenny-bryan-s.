﻿namespace StateStuff
{
    public class StateMachine<T>
    {
        public State<T> currentState { get; private set; }
        public T Owner;

        public StateMachine(T _o)
        {
            Owner = _o;
            currentState = null;
        }

        public void ChangeState(State<T> _newstate)
        {
            if(currentState != null)
                currentState.ExitState(ref Owner);
            currentState = _newstate;
            currentState.EnterState(ref Owner);
        }

        public void Update()
        {
            if (currentState != null)
                currentState.UpdateState(ref Owner);
        }
    }

    public abstract class State<T>
    {
        public abstract void EnterState(ref T _owner);
        public abstract void ExitState(ref T _owner);
        public abstract void UpdateState(ref T _owner);
    }
}