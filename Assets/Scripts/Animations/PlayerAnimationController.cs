﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    #region Attributes

    private Animator animator;

    private const string IDLE_BOOL = "Idle";
    private const string NORMALRUN_BOOL = "Normal Run";
    private const string ATTACKRUN_BOOL = "Attack Run";
    private const string ATTACK_BOOL = "Attack";
    private const string ATTACKSTANDBY_BOOL = "Attack Standby";
    private const string COMBO_BOOL = "Combo";
    private const string DRAWBLADE_BOOL = "Draw Blade";
    private const string PUTBLADE_BOOL = "Put Blade";
    private const string DAMAGE_BOOL = "Damage";
    private const string SKILL_BOOL = "Skill";
    private const string DEAD_BOOL = "Dead";

    #endregion

    #region Animate Functions

    public void AnimateIdle()
    {
        Animate(IDLE_BOOL);
    }

    public void AnimateNormalRun()
    {
        Animate(NORMALRUN_BOOL);
    }

    public void AnimateAttackRun()
    {
        Animate(ATTACKRUN_BOOL);
    }

    public void AnimateAttack()
    {
        Animate(ATTACK_BOOL);
    }

    public void AnimateAttackStandby()
    {
        Animate(ATTACKSTANDBY_BOOL);
    }

    public void AnimateCombo()
    {
        Animate(COMBO_BOOL);
    }

    public void AnimateDrawBlade()
    {
        Animate(DRAWBLADE_BOOL);
    }

    public void AnimatePutBlade()
    {
        Animate(PUTBLADE_BOOL);
    }

    public void AnimateDamage()
    {
        Animate(DAMAGE_BOOL);
    }

    public void AnimateSkill()
    {
        Animate(SKILL_BOOL);
    }

    public void AnimateDead()
    {
        Animate(DEAD_BOOL);
    }

    #endregion

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);

        animator.SetBool(boolName, true);
    }

    void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach(AnimatorControllerParameter parameter in animator.parameters)
        {
            if (parameter.name != animation) animator.SetBool(parameter.name, false);
        }
    }
}
