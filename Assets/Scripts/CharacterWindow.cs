﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterWindow : MonoBehaviour
{
    public GameObject unitObject;

    private HeroineClass unit;

    public Text name;
    public Text lvl;
    public Text hp;
    public Text atk;
    public Text vit;
    public Text str;
    public Text Int;
    public Text spr;
    public Text exp;
    public Text sp;

    public GameObject statButtons;

    // Use this for initialization
    void Start()
    {
        unit = unitObject.GetComponent<PlayerStats>().player;
    }

    // Update is called once per frame
    void Update()
    {
        name.text = "Name : " + unit.name;
        lvl.text = "LVL : " + unit.LVL;
        hp.text = "HP : " + unit.CurrentHP + " / " + unit.Stats.MaxHP;
        atk.text = "ATK : " + unit.BaseDamage();
        vit.text = "VIT : " + unit.VIT;
        str.text = "STR : " + unit.STR;
        Int.text = "INT : " + unit.INT;
        spr.text = "SPR : " + unit.SPR;
        exp.text = "EXP : " + unit.CurrentEXP;
        sp.text = "SP : " + unit.statPoints;

        //if (Input.GetMouseButton(1)) unit.CurrentEXP += 4;

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    Debug.Log("Space!");
        //    unit.Stats.MaxHP += 10;
        //}

        if (unit.statPoints >= 1) statButtons.SetActive(true);
        else statButtons.SetActive(false);
    }

    public void UpgradeVIT()
    {
        unit.Stats.VIT += 1;
        unit.Stats.MaxHP = unit.VIT * 10 + unit.LVL;
        unit.statPoints -= 1 /* * unit.LVL*/;
    }

    public void UpgradeSTR()
    {
        unit.Stats.STR += 1 + unit.LVL;
        unit.statPoints -= 1;
    }

    public void UpgradeINT()
    {
        unit.Stats.INT += 1 + unit.LVL;
        unit.statPoints -= 1;
    }

    public void UpgradeSPR()
    {
        unit.Stats.SPR += 1 + unit.LVL;
        unit.statPoints -= 1;
    }
}
