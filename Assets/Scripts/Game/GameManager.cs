﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject levelUpUI;

    private GameObject[] allEnemies;
    private GameObject enemy;

    private HeroineClass playerHeroine;

    private bool UI;

	// Use this for initialization
	void Start ()
    {
        playerHeroine = player.GetComponent<PlayerStats>().player;

        levelUpUI.SetActive(false);
        UI = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;

        allEnemies = GameObject.FindGameObjectsWithTag("Enemy");

		foreach (GameObject enemy in allEnemies)
        {
            if(enemy.GetComponent<EnemyStats>().monster.Alive == false)
            {
                playerHeroine.CurrentEXP += enemy.GetComponent<EnemyStats>().monster.EXPval;
            }
        }

        if (playerHeroine.CurrentEXP >= playerHeroine.MaxEXP)
        {
            Debug.Log("Level Up!");

            playerHeroine.LevelUp();        
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (UI == false)
            {
                UI = true;
                levelUpUI.SetActive(true);
            }
            else if (UI == true)
            {
                UI = false;
                levelUpUI.SetActive(false);
            }
        }
    }
}
