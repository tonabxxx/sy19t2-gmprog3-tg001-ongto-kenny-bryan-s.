﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public GameObject[] monsters;
    public Transform[] spawnPoints;

    public int maxMonsterCount = 10;

    public float spawnRate = 3f;
    private float lastSpawn = 0;

    private int monsterIndex;
    private int spawnIndex;


    // Use this for initialization
    void Start()
    {
        spawnIndex = Random.Range(0, spawnPoints.Length);
        monsterIndex = Random.Range(0, monsters.Length);
    }

    // Update is called once per frame
    void Update()
    {
        if (maxMonsterCount >= 10)
        {
            if (Time.time > spawnRate + lastSpawn)
            {
                GameObject monster = Instantiate(monsters[monsterIndex], spawnPoints[spawnIndex].transform.position, transform.rotation);

                spawnIndex = Random.Range(0, spawnPoints.Length);
                monsterIndex = Random.Range(0, monsters.Length);

                lastSpawn = Time.time;
            }
        }
    }
}
