﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideButtons : MonoBehaviour
{
    public GameObject playerAnimations;
    public GameObject monstersAnimations;
    public GameObject npcAnimations;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void PressedPlayer()
    {
        playerAnimations.SetActive(true);
        monstersAnimations.SetActive(false);
        npcAnimations.SetActive(false);
    }

    public void PressedMonsters()
    {
        monstersAnimations.SetActive(true);
        playerAnimations.SetActive(false);
        npcAnimations.SetActive(false);
    }

    public void PressedNPC()
    {
        npcAnimations.SetActive(true);
        playerAnimations.SetActive(false);
        monstersAnimations.SetActive(false);
    }
}
