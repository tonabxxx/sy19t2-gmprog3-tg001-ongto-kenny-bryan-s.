﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{
    //public GameObject enemyObject;

    public Image healthBar;
    private int MaxHP;
    private int CurHP;

    private MonsterClass enemyMonster;
    private GameObject grandParent;

    private Unit unit;

    private float fillAmount;

    private void Awake()
    {
        //healthBar = GetComponent<Image>();
        //enemyMonster = enemyObject.GetComponent<EnemyStats>().monster;

        grandParent = gameObject.transform.root.gameObject;

        
    }

    // Use this for initialization
    void Start ()
    {
        unit = grandParent.GetComponent<EnemyStats>().monster;
    }
    // Update is called once per frame
    void Update ()
    {
        //healthBar.fillAmount = grandParent.GetComponent<EnemyStats>().monster.CurrentHP / grandParent.GetComponent<EnemyStats>().monster.Stats.MaxHP;

        //healthBar.fillAmount = 0.50f;

        //Debug.Log("Fill Amount: " + healthBar.fillAmount);

        // grand parent
        //Debug.Log("Max HP: " + grandParent.GetComponent<EnemyStats>().monster.Stats.MaxHP);
        //Debug.Log("Current HP: " + grandParent.GetComponent<EnemyStats>().monster.CurrentHP);

        //fillAmount = grandParent.GetComponent<EnemyStats>().monster.CurrentHP / grandParent.GetComponent<EnemyStats>().monster.Stats.MaxHP;
        //Debug.Log("Supposed Fill: " + fillAmount);

        //Debug.Log(grandParent.GetComponent<EnemyStats>().monster.CurrentHP);

        //Debug.Log(grandParent.name);
    }

    private void LateUpdate()
    {
        //Debug.Log("Max HP: " + unit.MaxHP);
        //Debug.Log("Current HP: " + unit.CurrentHP);

        MonsterClass enemyMonster = grandParent.GetComponent<EnemyStats>().monster;

        healthBar.fillAmount = (float)enemyMonster.CurrentHP / (float)enemyMonster.Stats.MaxHP;

        //Debug.Log("Fill Amount: " + healthBar.fillAmount);
        //Debug.Log("Supposed Fill: " + fillAmount);

        //Debug.Log("Division: " + division);

        //Debug.Log("CUR HP: " + enemyMonster.CurrentHP);
        //Debug.Log("MAX HP: " + enemyMonster.Stats.MaxHP);
    }
}
