﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class EnemyStats : MonoBehaviour
{
    public MonsterClass monster;
    private NormalAttack normalAttack;

    public int expBaseVal;

    private void Awake()
    {
        monster = new MonsterClass();
        normalAttack = new NormalAttack();
    }

    // Use this for initialization
    void Start()
    {
        //expBaseVal = 25 * this.GetComponent<Stats>().LVL;
        //this.GetComponent<Unit>().EXPval = expBaseVal;



        monster.Initialize();
        monster.Skill.Add(normalAttack);

        monster.EXPval = expBaseVal * monster.LVL;


        //Debug.Log("EXP Value:" + monster.EXPval);
        //monster.PrintStats(); 
    }

    // Update is called once per frame
    void Update()
    {
        //healthBar.fillAmount = 10 / monster.Stats.MaxHP;

        //Debug.Log("Current HP: " + monster.CurrentHP);
        //Debug.Log("Max HP: " + monster.Stats.MaxHP);

        if (!monster.Alive) Die();
    }

    // Animation Event
    void DamageTo()
    {
        MonsterClass enemyMonster = this.gameObject.GetComponent<EnemyStats>().monster;
        HeroineClass playerHeroine = this.gameObject.GetComponent<AI>().player.gameObject.GetComponent<PlayerStats>().player;

        enemyMonster.Skill[0].activateSkill(enemyMonster, playerHeroine);
    }

    public void Die()
    {
        Destroy(this.gameObject);
    }
}
