﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroineClass : Unit
{
    public void Initialize()
    {
        Stats.Name = "Koko";

        Stats.VIT = 13;
        Stats.STR = BaseDamage() + VIT + STR;

        Stats.SPR = VIT + 2;
        Stats.INT = SPR;

        Stats.MATK = STR + SPR;

        Stats.MaxHP = Stats.VIT * 10;
        Stats.MaxMP = SPR + INT + VIT;

        Stats.MPRegen = Stats.MaxMP * 0.01f;

        Stats.LVL = 1;

        Stats.CurEXP = 0;
        Stats.MaxEXP = 100;

        CurrentHP = Stats.MaxHP;
        CurrentMP = Stats.MaxMP;

        gold = 0;
    }
}
