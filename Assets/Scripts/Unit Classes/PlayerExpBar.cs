﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerExpBar : MonoBehaviour
{
    public Image expBar;
    private int MaxEXP;
    private int CurEXP;

    public GameObject player;
    private HeroineClass playerClass;

    private Unit unit;

    private float fillAmount;

    private void Awake()
    {
        playerClass = player.GetComponent<PlayerStats>().player;
    }

    // Use this for initialization
    void Start ()
    {

    }
    // Update is called once per frame
    void Update ()
    {

    }

    private void LateUpdate()
    {
        playerClass = player.GetComponent<PlayerStats>().player;

        expBar.fillAmount = (float)playerClass.CurrentEXP / (float)playerClass.Stats.MaxEXP;

    }
}
