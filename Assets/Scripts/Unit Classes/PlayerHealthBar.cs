﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    //public GameObject enemyObject;

    public Image healthBar;
    private int MaxHP;
    private int CurHP;

    public GameObject player;
    private HeroineClass playerClass;

    private Unit unit;

    private float fillAmount;
    private float division;

    private void Awake()
    {
        playerClass = player.GetComponent<PlayerStats>().player;
    }

    // Use this for initialization
    void Start ()
    {

    }
    // Update is called once per frame
    void Update ()
    {
        //healthBar.fillAmount = grandParent.GetComponent<EnemyStats>().monster.CurrentHP / grandParent.GetComponent<EnemyStats>().monster.Stats.MaxHP;

        //healthBar.fillAmount = 0.50f;

        //Debug.Log("Fill Amount: " + healthBar.fillAmount);

        // grand parent
        //Debug.Log("Max HP: " + grandParent.GetComponent<EnemyStats>().monster.Stats.MaxHP);
        //Debug.Log("Current HP: " + grandParent.GetComponent<EnemyStats>().monster.CurrentHP);

        //fillAmount = grandParent.GetComponent<EnemyStats>().monster.CurrentHP / grandParent.GetComponent<EnemyStats>().monster.Stats.MaxHP;
        //Debug.Log("Supposed Fill: " + fillAmount);

        //Debug.Log(grandParent.GetComponent<EnemyStats>().monster.CurrentHP);

        //Debug.Log(grandParent.name);

        //if (Input.GetMouseButton(0)) playerClass.CurrentHP -= 10;
    }

    private void LateUpdate()
    {
        //Debug.Log("Max HP: " + unit.MaxHP);
        //Debug.Log("Current HP: " + unit.CurrentHP); 

        //MonsterClass enemyMonster = grandParent.GetComponent<EnemyStats>().monster;

        playerClass = player.GetComponent<PlayerStats>().player;

        healthBar.fillAmount = (float)playerClass.CurrentHP / (float)playerClass.Stats.MaxHP;

        //Debug.Log("Fill Amount: " + healthBar.fillAmount);
        //Debug.Log("Supposed Fill: " + fillAmount);

        //Debug.Log("Division: " + division);

        //Debug.Log("CUR HP: " + enemyMonster.CurrentHP);
        //Debug.Log("MAX HP: " + enemyMonster.Stats.MaxHP);
    }
}
