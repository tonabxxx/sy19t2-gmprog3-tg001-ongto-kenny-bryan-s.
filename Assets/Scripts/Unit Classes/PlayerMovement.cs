﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    private NavMeshAgent agent;

    public Animator animator;
    private PlayerAnimationController animationController;

    public float moveSpeed = 10;
    public float rotSpeed = 5;

    public GameObject marker;
    public Vector3 offset = new Vector3(0, 0, 0);

    private Vector3 targetPosition;
    private GameObject enemyObject;

    private bool isIdle;
    private bool isMovingToMarker;
    private bool isMovingToEnemy;
    private bool isAttacking;
    private bool isAttackingStandby;
    public bool isSkilling;

    private Vector3 lookAtTarget;
    private Quaternion playerRot;

    private GameObject point;
    private GameObject interactableObject;

    public float meleeRange = 3f;

    private PlayerStats playerStats;

    public float movementSpeed;

    private bool isStopped = false;

    // Use this for initialization
    void Start ()
    {
        animationController = GetComponent<PlayerAnimationController>();

        targetPosition = transform.position;

        isMovingToMarker = false;
        isMovingToEnemy = false;
        isAttacking = false;
        isAttackingStandby = false;
        isIdle = false;

        playerStats = GetComponent<PlayerStats>();

        agent = this.GetComponent<NavMeshAgent>();

        agent.speed = moveSpeed;
    }
	
	// Update is called once per frame
	void Update ()
    {
        ClickToMove();
        if (isIdle)
        {
            agent.isStopped = true;
            animationController.AnimateIdle();
        }

        else if (isMovingToMarker)
        {
            agent.isStopped = false;
            animationController.AnimateNormalRun();
            MoveToMarker();
        }

        else if (isMovingToEnemy)
        {
            agent.isStopped = false;
            animationController.AnimateAttackRun();
            MoveToAttack();
        }

        else if (isAttacking)
        {
            agent.isStopped = true;
            animationController.AnimateAttack();
            if (enemyObject != null)
            {
                playerRot = Quaternion.LookRotation(enemyObject.transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, rotSpeed * Time.deltaTime);
            }
            if (enemyObject == null)
            {
                isAttacking = false;
                isAttackingStandby = true;
            }
        }

        else if (isAttackingStandby)
        {
            agent.isStopped = true;
            animationController.AnimateAttackStandby();
        }

        if (isSkilling)
        {
            agent.isStopped = true;
            animationController.AnimateSkill();
        }
	}

    void DamageTo(string value)
    {
        MonsterClass enemyMonster = enemyObject.GetComponent<EnemyStats>().monster;
        HeroineClass playerHeroine = this.gameObject.GetComponent<PlayerStats>().player;

        //Debug.Log("ENEMY HP: " + enemyMonster.CurrentHP);

        playerHeroine.Skill[0].activateSkill(playerHeroine, enemyMonster);
    }

    void StartEffect(string value)
    {

    }

    void StopEffect(string value)
    {

    }

    void AreaAttack(string value)
    {
        //Debug.Log("Area Attack here");
        HeroineClass playerHeroine = this.gameObject.GetComponent<PlayerStats>().player;

        playerHeroine.Skill[1].activateSkill(playerHeroine, playerHeroine);

        isSkilling = false;
    }

    void ClickToMove()
    {
        if (Input.GetMouseButtonDown(1))
        {
            isIdle = false;
            Destroy(point);
            SetTargetPosition();
        }
    }

    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.transform != null)
            {
                if (hit.transform.gameObject.tag == "Ground")
                {
                    MoveToPosition(hit);
                    //interactableObject = null;
                    if (enemyObject != null) enemyObject.transform.GetChild(0).gameObject.SetActive(false);
                    else if (interactableObject != null) interactableObject.transform.GetChild(0).gameObject.SetActive(false);
                }
                else if (hit.transform.gameObject.tag == "Interactable")
                {
                    MoveToPosition(hit);
                    if (enemyObject != null) enemyObject.transform.GetChild(0).gameObject.SetActive(false);
                }
                else if (hit.transform.gameObject.tag == "Enemy")
                {
                    MoveToEnemy(hit);
                    if (interactableObject != null) interactableObject.transform.GetChild(0).gameObject.SetActive(false);
                }
                
            }
        }
    }

    void MoveToPosition(RaycastHit hit)
    {
        targetPosition = hit.point;

        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
                                   transform.position.y,
                                   targetPosition.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        isMovingToMarker = true;

        if (hit.transform.gameObject.tag == "Ground")
        {
            
            point = Instantiate(marker, targetPosition + offset, transform.rotation);
        }
        else if (hit.transform.gameObject.tag == "Interactable")
        {
            interactableObject = hit.transform.gameObject;
            interactableObject.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    void MoveToEnemy(RaycastHit hit)
    {
        targetPosition = hit.point;

        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
                                transform.position.y,
                                targetPosition.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        isMovingToMarker = false;
        isMovingToEnemy = true;

        targetPosition = hit.transform.gameObject.transform.position;
        enemyObject = hit.transform.gameObject;

        // Enemy Marker
        enemyObject.transform.GetChild(0).gameObject.SetActive(true);
    }

    void MoveToAttack()
    {
        enemyObject.transform.GetChild(0).gameObject.SetActive(true);

        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, rotSpeed * Time.deltaTime);

        agent.SetDestination(enemyObject.transform.position);

        if (Vector3.Distance(enemyObject.transform.position, agent.transform.position) <= meleeRange)
        {
            isMovingToEnemy = false;
            isAttacking = true;
        }

    }

    void MoveToMarker()
    {
        
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, rotSpeed * Time.deltaTime);
        //transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);

        agent.SetDestination(targetPosition);

        //if (interactableObject == null)
        //{
        //    agent.isStopped = true;
        //}

        if (Vector3.Distance(transform.position, targetPosition) <= 0.5f)
        {
            //Debug.Log("I arrived");
            Destroy(point);
            isMovingToMarker = false;
            isIdle = true;
        }
        else if (interactableObject == null) return;
        else if (interactableObject != null)
        {
            if (Vector3.Distance(transform.position, targetPosition) <= 1.75f)
            {
                //Debug.Log("I arrived");
                Destroy(point);
                isMovingToMarker = false;
                isIdle = true;
            }
        }


    }
}
