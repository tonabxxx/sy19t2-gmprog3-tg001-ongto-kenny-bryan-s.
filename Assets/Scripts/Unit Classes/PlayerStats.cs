﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public HeroineClass player;
    private NormalAttack normalAttack;

    public bool enableStartingStats;

    public int startingHP;
    public int startingMP;
    public int startingSP;

    private float manaRate = 0.5f;
    private float lastRegen = 0.0f;

    private void Awake()
    {

        player = new HeroineClass();
        normalAttack = new NormalAttack();
    }

    // Use this for initialization
    void Start()
    {
        player.Initialize();

        player.Skill.Add(normalAttack);
        //player.PrintStats();

        if (enableStartingStats)
        {
            player.CurrentHP = startingHP;
            player.CurrentMP = startingMP;
            player.statPoints = startingSP;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //player.CurrentMP += player.Stats.MPRegen;

        //Debug.Log(player.Stats.MPRegen);

        if (Time.time > manaRate + lastRegen)
        {
            player.CurrentMP += player.Stats.MPRegen;
            lastRegen = Time.time;
        }
    }


    public Transform playerTransform()
    {
        return gameObject.transform;
    }
}
