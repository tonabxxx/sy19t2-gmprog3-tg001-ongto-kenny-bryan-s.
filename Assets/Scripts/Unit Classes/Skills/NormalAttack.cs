﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalAttack : Skill
{
    // Sets the Name of the skill and MP Cost
    public NormalAttack()
    {
        Name = "Basic Attack";
        MpCost = 0;
    }

    public override void activateSkill(Unit actor, Unit target)
    {
        int damage = actor.Damage(target);
        target.TakeDamage(damage);
    }
}
