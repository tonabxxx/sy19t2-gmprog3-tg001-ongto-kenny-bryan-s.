﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill
{
    protected PlayerAnimationController animationController;

    // Cost of the skill
    public int MpCost { get; set; }
    // Cooldown of the skill
    public int Cooldown { get; set; }
    public int ActiveCooldown { get; set; }
    // Name of the skill
    public string Name { get; set; }

    public int amount { get; set; }
    public bool isActive { get; set; }

    public float AOEradius { get; set; }

    public virtual void activateSkill(Unit actor, Unit target) { }

    public virtual void deactivateSkill(Unit actor, Unit target) { }

    public void AOE(Unit actor, int _damage, float AOEradius)
    {
        Vector3 position = actor.PlayerTransform();

        //Debug.Log(position);
        //Debug.Log(actor.name); // KOKO CONFIRMED

        Collider[] colliders = Physics.OverlapSphere(position, AOEradius);
        foreach (Collider collider in colliders)
        {
            //if (collider.tag == "Enemy") Damage(collider.transform, _damage);
            if (collider.tag == "Enemy")
            {
                //Debug.Log("Enemy received " + _damage + " damage");
                Damage(collider.transform, _damage);
            }
        }
    }

    public void Damage (Transform target, int _damage)
    {
        EnemyStats enemy = target.GetComponent<EnemyStats>();

        Debug.Log(enemy.name);

        enemy.monster.TakeDamage(_damage);
    }
}
