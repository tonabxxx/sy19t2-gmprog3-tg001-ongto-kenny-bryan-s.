﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Stats
{
    public string Name { get; set; }
    public float MaxHP { get; set; }
    public float CurHP { get; set; }
    public float MaxMP { get; set; }
    public float CurMP { get; set; }
    public int VIT { get; set; }
    public int STR { get; set; }
    public int INT { get; set; }
    public int SPR { get; set; }
    public int LVL { get; set; }
    public int MATK { get; set; }
    public float MPRegen { get; set; }
    public float MaxEXP { get; set; }
    public float CurEXP { get; set; }
}
