﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit
{
    public Stats Stats;

    public List<Skill> Skill { get; set; }
    public Unit() { Skill = new List<Skill>(); }

    public int statPoints;

    public int gold;

    private float currentHP;

    public float CurrentHP
    {
        get { return currentHP; }
        set
        {
            // Clamps HP
            currentHP = value;
            if (currentHP < 0) currentHP = 0;
            if (currentHP > Stats.MaxHP) currentHP = Stats.MaxHP;
        }
    }

    private float maxHP;

    public float MaxHP
    {
        get { return Stats.MaxHP; }
        set { Stats.MaxHP = Stats.VIT * 10; }
    }

    private float currentMP;
    public float CurrentMP
    {
        get { return currentMP; }
        set
        {
            // Clamps MP
            currentMP = value;
            if (currentMP < 0) currentMP = 0;
            if (currentMP > Stats.MaxMP) currentMP = Stats.MaxMP;
        }
    }

    //public int MaxEXP
    public float MaxEXP
    {
        get { return Stats.MaxEXP; }
        set { }
    }

    public string name
    {
        get { return Stats.Name; }
        set { }
    }

    public int VIT
    {
        get { return Stats.VIT; }
        set { }
    }

    public int STR
    {
        get { return Stats.STR; }
        set { }
    }

    public int INT
    {
        get { return Stats.INT; }
        set { }
    }

    public int SPR
    {
        get { return Stats.SPR; }
        set { }
    }

    private int currentEXP;
    public int CurrentEXP
    {
        get { return currentEXP; }
        set
        {
            // Clamps EXP
            currentEXP = value;
            if (currentEXP < 0) currentEXP = 0;
            //if (currentEXP > MaxEXP) LevelUp();
        }
    }

    private int currentLVL;
    public int LVL
    {
        get { return Stats.LVL; }
        set { }
    }

    public int EXPval;

    public int BaseDamage()
    {
        int randomVariance = Random.Range(0, 20);
        randomVariance = randomVariance / 100;
        int baseDamage = Stats.STR + (int)(randomVariance * Stats.STR);
        return baseDamage;
    }

    public int Damage(Unit Target)
    {
        //Console.WriteLine("Hi");
        int damage = (int)((BaseDamage() - Target.Stats.VIT));
        if (damage <= 0) damage = 1;
        return damage;
    }

    public void TakeDamage(int damage)
    {
        currentHP -= damage;
        if (currentHP <= 0) currentHP = 0;

    }

    public bool Alive
    {
        get
        {
            if (CurrentHP <= 0) return false;
            else return true;
        }
    }

    public void LevelUp()
    {
        Stats.LVL++;
        CurrentEXP = 0;
        MaxEXP = (int) ((LVL / 1.5f) * MaxEXP);

        statPoints += (int) 1 * (LVL / (1 + LVL / LVL / LVL) + 3);
    }

    public void PrintStats()
    {
        Debug.Log("LVL: " + Stats.LVL);
        Debug.Log("VIT: " + Stats.VIT);
        Debug.Log("STR: " + Stats.STR);
        Debug.Log("HP: " + currentHP);
        Debug.Log("MP: " + currentMP);
        Debug.Log("EXP VAL: " + EXPval);
    }

    public Vector3 PlayerTransform()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector3 playerVector = new Vector3(player.transform.position.x, player.transform.position.y);
        return playerVector;
    }

    public GameObject PlayerGameObject()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        return player;
    }
}
